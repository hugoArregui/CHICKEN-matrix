(module matrix

  (
   make-vector-matrix
   vector-matrix?
   vector->vector-matrix

   matrix?
   matrix->submatrix
   matrix=?
   matrix-eq?
   matrix-get
   matrix-set!
   matrix-shape=?
   matrix-equal?
   matrix-pp
   matrix-shape
   matrix-map
   matrix-map!
   matrix-transpose
   matrix-row
   matrix-column
   matrix-for-each
   matrix-square?)

  (import chicken scheme matchable)
  (use loops symbol-utils data-structures srfi-13)
  (use fast-generic sequences srfi-1 vector-lib)

  (define (make-vector-with-iterator size f #!key (default #f))
    (let ((v (make-vector size default)))
      (vector-map! f v)
      v))

  (include "strategies/vector-matrix.scm")

  (define (matrix? m)
    (vector-matrix? m))

  (define-type <matrix> matrix?)

  (define (matrix-for-each f m . ms)
    (matrix-for-each-imple f (cons m ms)))

  (define (matrix-map f m . ms)
    (matrix-map-imple f (cons m ms)))

  (define-generic (matrix-shape=? (<matrix> m1) (<matrix> m2))
    (equal? (matrix-shape m1) (matrix-shape m2)))

  (define-generic (matrix-square? (<matrix> m))
    (match-let (((x-len . y-len) (matrix-shape m)))
      (= x-len y-len)))

  (define-generic (matrix=? elem=? (<matrix> m1) (<matrix> m2))
    (if (not (matrix-shape=? m1 m2))
      #f
      (call/cc
        (lambda (exit)
          (matrix-for-each
            (lambda (i j value1 value2)
              (if (not (elem=? value1 value2))
                (exit #f)))
            m1
            m2)
          #t))))

  (define-generic (matrix-eq? (<matrix> m1) (<matrix> m2))
    (matrix=? eq? m1 m2))

  (define-generic (matrix-equal? (<matrix> m1) (<matrix> m2))
    (matrix=? equal? m1 m2))

  (define-generic (matrix-row (<vector-matrix> m) n)
    (assert (>= n 0) "invalid row index")
    (match-let (((x-len . y-len) (matrix-shape m)))
      (assert (< n y-len) "invalid row index")
      (make-vector-with-iterator x-len
                                 (lambda (i e)
                                   (matrix-get m i n)))))

  (define-generic (matrix-pp (<vector-matrix> m))
    (vector-for-each
     (lambda (_ row)
       (display (string-join (map ->string (vector->list row)) " "))
       (newline))
     (vector-matrix-data m)))

  (define-generic (matrix-column (<vector-matrix> m) n)
    (assert (>= n 0) "invalid column index")
    (match-let (((x-len . y-len) (matrix-shape m)))
      (assert (< n x-len) "invalid column index")
      (make-vector-with-iterator y-len
                                 (lambda (i e)
                                   (matrix-get m n i)))))
  )
