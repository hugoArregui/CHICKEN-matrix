(use matrix)
(use (except test test-error) test-error-assert symbol-utils)

(define (test-matrix-eq? expected m)
  (test-assert (matrix-eq?
                 (if (vector? expected)
                   (vector->vector-matrix expected)
                   expected)
                 m)))

(test-group "success"
  (test-group "make from sizes and no default"
    (let ((m (make-vector-matrix 2 3))
          (u (unspecified-value)))
      (test "shape" '(2 . 3) (matrix-shape m))
      (matrix-for-each (lambda (i j v)
                         (test-assert (unspecified? v)) )
                       m)
      (test-assert (not (matrix-square? m)))
      (test-assert (vector-matrix? m))
      (test-assert (matrix? m))))

  (test-group "make from sizes and default"
    (let ((m (make-vector-matrix 2 2 3)))
      (test "shape" '(2 . 2) (matrix-shape m))
      (matrix-for-each (lambda (i j v)
                         (test 3 v))
                       m)
      (test-assert (matrix-square? m))
      (test-assert (vector-matrix? m))
      (test-assert (matrix? m))))

  (test-group "from data"
    (let* ((data '#(#(3 3)
                    #(3 3)))
           (m     (vector->vector-matrix data)))
      (test "shape" '(2 . 2) (matrix-shape m))
      (matrix-for-each (lambda (i j v)
                         (test 3 v))
                       m)
      (test-assert (matrix-square? m))
      (test-assert (vector-matrix? m))
      (test-assert (matrix? m))))

  (test-group "get/set"
    (let ((m (make-vector-matrix 2 2 3)))
      (test 3 (matrix-get m 0 0))
      (matrix-set! m 0 0 2)
      (test 2 (matrix-get m 0 0))))

  (test-group "matrix=?"
    (let ((m1 (make-vector-matrix 2 2 3))
          (m2 (make-vector-matrix 2 1 3))
          (m3 (make-vector-matrix 2 2 2))
          (m4 (make-vector-matrix 2 2 3)))
      (test-assert (not (matrix-eq? m1 m2)))
      (test-assert (not (matrix-eq? m1 m3)))
      (test-assert (matrix-eq? m1 m4))))

  (test-group "map"
    (let* ((m  (make-vector-matrix 2 2 3))
           (m2 (matrix-map (lambda (i j v)
                             (if (= i j) 1 0)) m)))
      (test-matrix-eq? '#(#(1 0)
                          #(0 1)) m2)
      (test-matrix-eq? '#(#(3 0)
                          #(0 3))
                       (matrix-map (lambda (i j v1 v2 v3)
                                     (+ v1 v2 v3))
                                   m2 m2 m2))))

  (test-group "map!"
    (let ((m (make-vector-matrix 2 2 3)))
      (matrix-map! (lambda (i j v)
                     (if (= i j) 1 0)) m)
      (test-matrix-eq? '#(#(1 0)
                          #(0 1)) m)))

  (test-group "matrix-transpose"
    (let ((m (make-vector-matrix 2 2 3)))
      (matrix-map! (lambda (i j v)
                     (if (= i j) 1 0)) m)
      (test-matrix-eq? '#(#(1 5)
                          #(2 6)
                          #(3 7)
                          #(4 8))
                       (matrix-transpose
                        (vector->vector-matrix '#(#(1 2 3 4)
                                                  #(5 6 7 8)))))))

  (test-group "matrix-column"
    (let ((m (vector->vector-matrix
              '#(#(10 9 2 5)
                 #(13 3 9 1)
                 #(11 0 0 2)))))
      (test '#(9 3 0) (matrix-column m 1))))

  (test-group "matrix-row"
    (let ((m (vector->vector-matrix '#(#(10 9 2 5)
                               #(13 3 9 1)
                               #(11 0 0 2)))))
      (test '#(13 3 9 1) (matrix-row m 1))))

  (test-group "matrix->submatrix"
    (let ((m (vector->vector-matrix '#(#(10 9 2 5)
                               #(13 3 9 1)
                               #(11 0 0 2)))))

      (test-matrix-eq? '#(#(10 9)
                          #(13 3))
                       (matrix->submatrix m 0 0 2 2))

      (test-matrix-eq? '#(#(10 9 2))
                       (matrix->submatrix m 0 0 3 1))

      (test-matrix-eq? m
                       (matrix->submatrix m 0 0 4 3))

      (test-matrix-eq? '#(#(9 1)
                          #(0 2))
                       (matrix->submatrix m 2 1 2 2))))

  ) ; end success test

(test-group "failures"
  (test-group "create from sizes"
    (test-error "invalid x-size"
                (make-vector-matrix -1 10)
                (('exn ('message "invalid shape"))))
    (test-error "invalid y-size"
                (make-vector-matrix 1 -1)
                (('exn ('message "invalid shape")))))

  (test-group "create from data"
    (test-error "invalid data"
                (vector->vector-matrix '())
                (('exn ('message "invalid data")))))

  (let ((m3x3 (make-vector-matrix 3 3)))
    (test-group "matrix-set!"
      (test-error "x negative"
                  (matrix-set! m3x3 -1 0 #f)
                  (('exn ('message "x out of range"))))
      (test-error "y negative"
                  (matrix-set! m3x3 0 -1 #f)
                  (('exn ('message "y out of range"))))
      (test-error "x out of bonds"
                  (matrix-set! m3x3 3 0 #f)
                  (('exn ('message "x out of range"))))
      (test-error "y out of bonds"
                  (matrix-set! m3x3 0 3 #f)
                  (('exn ('message "y out of range")))))

    (test-group "matrix-column"
      (test-error "negative column index"
                  (matrix-column m3x3 -1)
                  (('exn ('message "invalid column index"))))
      (test-error "out of bounds column index"
                  (matrix-column m3x3 3)
                  (('exn ('message "invalid column index")))))

    (test-group "matrix-row"
      (test-error "negative row index"
                  (matrix-row m3x3 -1)
                  (('exn ('message "invalid row index"))))
      (test-error "out of bounds row index"
                  (matrix-row m3x3 3)
                  (('exn ('message "invalid row index"))))))

  ) ; end failures test

(test-exit)
