(define-record vector-matrix x-size y-size data)

(define-type <vector-matrix> vector-matrix?)
(define-type <list-of-vector-matrix> (lambda (e)
                                       (and (list? e)
                                            ((o not empty?) e)
                                            (all? vector-matrix? e))))

(define wrap-data make-vector-matrix)

(define (raw-position m x y)
  (match-let (((x-len . y-len) (matrix-shape m)))
             (+ (* y x-len) x)))

(define (make-vector-matrix x-size y-size
                            #!optional (default (unspecified-value)))
  (assert (and (positive? x-size)
               (positive? y-size)) "invalid shape")
   (wrap-data x-size y-size (make-vector (* x-size y-size) default)))

(define (vector->vector-matrix data)
  (assert
   (and (vector? data)
        ((o not vector-empty?) data))
            "invalid data")
  (let ((x-size (vector-length (vector-ref data 0)))
        (y-size (vector-length data)))
    (assert (and
             (> x-size 0)
             (vector-every (lambda (v)
                             (and
                              (vector? v)
                              (= (vector-length v) x-size)))
                           data))
            "invalid data")
  (let ((m (make-vector-matrix x-size y-size)))
    (matrix-for-each
     (lambda (i j v)
       (matrix-set! m i j
                    (vector-ref (vector-ref data j) i)))
     m)
    m)))

(define-generic (matrix-shape (<vector-matrix> m))
  (cons (vector-matrix-x-size m)
        (vector-matrix-y-size m)))

(define-generic (matrix-set! (<vector-matrix> m) x y value)
  (match-let (((x-len . y-len) (matrix-shape m)))
    (assert (and (>= x 0) (< x x-len)) "x out of range")
    (assert (and (>= y 0) (< y y-len)) "y out of range")
    (vector-set! (vector-matrix-data m) (raw-position m x y) value)
    m))

(define-generic (matrix-get (<vector-matrix> m) x y)
  (match-let (((x-len . y-len) (matrix-shape m)))
    (assert (and (>= x 0) (< x x-len)) "x out of range")
    (assert (and (>= y 0) (< y y-len)) "y out of range")
    (vector-ref (vector-matrix-data m) (raw-position m x y))))

(define-generic (matrix-for-each-imple f (<list-of-vector-matrix> ms))
  (match-let* ((m               (car ms))
               ((x-len . y-len) (matrix-shape m)))
    (assert (all? (cut matrix-shape=? m <>) (cdr ms)))
    (do-times i x-len
      (do-times j y-len
        (apply f i j (map (cut matrix-get <> i j) ms))))))

(define-generic (matrix-map-imple f (<list-of-vector-matrix> ms))
  (match-let* ((m               (car ms))
               ((x-len . y-len) (matrix-shape m))
               (new-m           (make-vector-matrix x-len y-len)))
    (apply matrix-for-each
           (lambda (i j . values)
             (matrix-set! new-m i j (apply f i j values)))
           ms)
    new-m))

(define-generic (matrix-map! f (<vector-matrix> m))
  (match-let (((x-len . y-len) (matrix-shape m)))
    (matrix-for-each
      (lambda (i j value)
        (matrix-set! m i j (f i j value)))
      m)))

(define-generic (matrix->submatrix (<vector-matrix> m)
                                   start-x start-y x-offset y-offset)
  (let ((sub (make-vector-matrix x-offset y-offset)))
    (do-times i x-offset
      (do-times j y-offset
                (matrix-set! sub i j
                             (matrix-get m
                                         (+ start-x i)
                                         (+ start-y j)))))
    sub))

(define-generic (matrix-transpose (<vector-matrix> m))
  (match-let (((x-len . y-len) (matrix-shape m)))
    (let ((new-m (make-vector-matrix y-len x-len)))
      (matrix-for-each (lambda (i j value)
                         (matrix-set! new-m j i value))
                       m)
      new-m)))
